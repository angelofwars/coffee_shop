// let burger_id = document.getElementById("burger")
// let nav_id = document.getElementById("nav")

// Переменные footer
const burger_id = document.querySelector('#burger')
const nav_id = document.querySelector('#nav')

// valid submit
const submit = document.querySelector('#submit')
const success_id = document.querySelector('#success')
const error_id = document.querySelector('#error')
const name_id = document.querySelector('#name')
const email_id = document.querySelector('#email')
const message_id = document.querySelector('#message')


const EMAIL_REGEXP = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu;


// footer animation

burger_id.addEventListener('click', () =>  {
    burger_id.classList.toggle("header__burger_active")
})

nav_id.addEventListener('click', () => {
    nav_id.classList.toggle('navigation__active')
})

// valid and animation submit

function valid_form (

) {
    if (
        // name_id.checkValidity() && Не работают
        // email_id.checkValidity() &&
        // message_id.checkValidity()
        !(name_id.value == '' ||
        email_id.value == '' ||
        message_id.value == '')
    ) {
        success_id.classList.add('wrapper-success_active')
        error_id.classList.remove('from__fields-error_active')

        // name_id.target.reset() Почему это не работает???
        // email_id.target.reset() Почему это не работает???
        // message_id.target.reset() Почему это не работает???
        setTimeout(() => {
            name_id.value = ''
            email_id.value = ''
            message_id.value = ''
            success_id.classList.remove('wrapper-success_active')

        }, 3e3)
    } else {
        error_id.classList.add('from__fields-error_active')
    }
}

name_id.addEventListener('change', (e) => {
    if (name_id) {
        name_id.style.borderColor = 'green';
    } else {
        name_id.style.borderColor = 'red';
        error_id.classList.add('form__fields-error_active')
    }

})

email_id.addEventListener('change', (e) => {
    // console.log('Написал email')
    if (isEmailValid(email_id.value)) {
        email_id.style.borderColor = 'green';
    } else {
        email_id.style.borderColor = 'red';
        error_id.classList.add('from__fields-error_active') // - Почему не работает?
    }

});



message_id.addEventListener('change', (e) => {
    if (message_id) {
        message_id.style.borderColor = 'green';
    } else {
        message_id.style.borderColor = 'red';
        error_id.classList.add('.form__fields-error_active')
    }
})
function isEmailValid(value) {
    return EMAIL_REGEXP.test(value);
};

submit.addEventListener('click', (e) => {
    e.preventDefault();
    valid_form()
})





